import React, { Component } from 'react'
import ReactDataList from './react-datalist/ReactDataList'
import GroupstageScheme from './GroupstageScheme'
import FinalsScheme from './FinalsScheme'
import {BootstrapTable, TableHeaderColumn, SizePerPageDropDown } from 'react-bootstrap-table'
import DatePicker from 'react-bootstrap-date-picker'
import { addTournamentSchema, searchTournamentQ, findTournamentQ, addTournamentQ, updateTournamentQ, tournamentsQ, deleteTournament } from '../schemas/addTournament'
import { findPlayerQ } from '../schemas/addPlayer'
import { searchTeamQ } from '../schemas/addTeam'
import MakeForm from './MakeForm'
import { MakeLabelListJSX } from './Dropdown_func'
import ShowSearch from './ShowSearch'

import InputValidation from './InputValidation'

export default class AddTournament extends Component {
    constructor( props ) {
        super(props);
        this.state = {
            startDate: new Date().toISOString(),
            endDate: new Date().toISOString(),

            editTournament: false,
            searchResults: null,
            addTournamentSchema,
            inputFieldsValue: {},
            idToUpdate: '',
            selectedSearch: '',
            selectedData: {},
            showDelete: false,

            editGroup: null,
            editFinals: null,

            groupsDone: false,
            groupsData: {},

            teamsToFinals: 0,

            finalsDone: false,
            finalsData: {},

            playerSearchResults: [],
            playerList: [],
            playerListLabels: [],

            teamSearchResults: [],
            teamList: [],
            teamListLabels: [],

            matchesSearchResults: [],
            matchesList: [],
            matchesListLabels: [],

            relatedTournamentsSearchResults: [],
            relatedTournamentsList: [],
            relatedTournamentsLabels: [],

            dataListOptionPlayer: [],
            dataListOptionTeam: [],
            dataListOptionMaches: [],
            dataListOptionRelatedTournaments: []
        };
        this.toggleAptDisplay = this.toggleAptDisplay.bind(this)
        this.onChangeHandeler = this.onChangeHandeler.bind(this)
        this.handelAdd = this.handelAdd.bind(this)
        this.onClickSearchRow  = this.onClickSearchRow.bind(this)
        this.editSearchRow  = this.editSearchRow.bind(this)
        this.clearIputFields  = this.clearIputFields.bind(this)
        this.removeLabel  = this.removeLabel.bind(this)
        this.showAll  = this.showAll.bind(this)
        this.deleteThisTournament  = this.deleteThisTournament.bind(this)
        this.dlOptionSelected  = this.dlOptionSelected.bind(this)
        this.onChangeDatalist  = this.onChangeDatalist.bind(this)
        this.returnGroupInfo  = this.returnGroupInfo.bind(this)
        this.returnFinalInfo  = this.returnFinalInfo.bind(this)
        this.startDateChange  = this.startDateChange.bind(this)
        this.endDateChange  = this.endDateChange.bind(this)
    }

    onChangeHandeler ( event ) {

        const { addTournamentSchema, playerSearchResults, editTournament, inputFieldsValue } = this.state
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = parseInt( target.name )
        const inputName = target.id

        const searchable = addTournamentSchema[name].searchable

        inputFieldsValue[inputName] = value
        const validationSearch = InputValidation( addTournamentSchema, this.refs, false, false )
        const inputItems = validationSearch.inputItems

        this.setState({
            addTournamentSchema,
            inputFieldsValue
        })

        if ( Object.keys(inputItems).length >= 1 && searchable && !editTournament && value.length > 1 ){
            this.props.searchForm( searchTournamentQ, inputItems )
            .then(result => {
                const data = JSON.parse(result.body).data.searchTournament
                this.ShowSearchResults( data )
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else if ( Object.keys(inputItems).length === 0 )
        {
            this.setState({
                searchResults: null
            })
        }
    }

    removeLabel (event)
    {
        const target = event.target
        const name = event.target.getAttribute('data-id')
        const type = event.target.getAttribute('data-name')

        let {
                  playerList
                , playerListLabels
                , teamList
                , teamListLabels
            } = this.state

        if (type === 'player')
        {
            delete playerList[name]
            delete playerListLabels[name]
        }

        if (type === 'team')
        {
            delete teamList[name]
            delete teamListLabels[name]
        }

        this.setState({
            playerList,
            playerListLabels,
            teamList,
            teamListLabels
        })

    }

    onClickSearchRow (selectedData, isSelected, event)
    {
        if ( this.state.selectedSearch !== selectedData.id || this.state.selectedData !== selectedData )
        {
            this.setState({
                selectedSearch: selectedData.id,
                selectedData: selectedData
            })
        }
    }

    editSearchRow( )
    {
        this.clearIputFields()
        let { selectedData, inputFieldsValue, selectedSearch } = this.state
        const { players, teams, matches, relatedTournaments, format, startDate, endDate } = selectedData

        console.log( )

        let playerList = [],
        playerListLabels = [],

        teamList = [],
        teamListLabels = [],

        matchesList = [],
        matchesListLabels = [],

        relatedTournamentsList = [],
        relatedTournamentsListLabels = [],

        editGroup = {},
        editFinals = {}

        if ( players !== null )
        {
            for (var i = players.length - 1; i >= 0; i--) {
                playerList[i] = { accountId : players[i].accountId }
                playerListLabels[i] = {
                    id : players[i].accountId,
                    name:  players[i].nickName
                }
            }
        }

        if ( teams !== null )
        {
            for (var i = teams.length - 1; i >= 0; i--) {
                teamList[i] = { steamId : teams[i].steamId }
                teamListLabels[i] = {
                    id : teams[i].steamId,
                    name:  teams[i].name
                }
            }
        }

        if ( matches !== null )
        {
            for (var i = matches.length - 1; i >= 0; i--)
            {
                matchesList[i] = { match_id : matches[i].match_id }
                matchesListLabels[i] = {
                    id : matches[i].match_id,
                    name:  ''
                }
            }
        }

        if ( relatedTournaments !== null )
        {
            for (var i = relatedTournaments.length - 1; i >= 0; i--)
            {
                relatedTournamentsList[i] = { match_id : relatedTournaments[i].leagueid }
                relatedTournamentsListLabels[i] = {
                    id : relatedTournaments[i].match_id,
                    name:  ''
                }
            }
        }

        if ( format !== null )
        {
            console.log( "format >>", format )
            editGroup.edit = format.groupStage
            editFinals.edit = format.finalStage
            selectedData.nrTeams = format.teamAmount
        }



        this.setState({

            startDate,
            endDate,

            inputFieldsValue: selectedData,
            editTournament: true,
            searchResults: null,
            idToUpdate: selectedSearch,
            showDelete: true,

            playerList,
            playerListLabels,

            teamList,
            teamListLabels,

            matchesList,
            matchesListLabels,

            relatedTournamentsList,
            relatedTournamentsListLabels,

            editGroup,
            editFinals
        })
    }

    showAll ()
    {
        this.props.searchForm( tournamentsQ )
        .then(result => {
            const data = JSON.parse(result.body).data.tournaments
            this.ShowSearchResults( data )
        })
        .catch(error => {
            console.log('onChangeHandeler error >', error)
        })
    }

    ShowSearchResults (data)
    {
        if (data.length !== 0 && data[0] !== null )
        {
            this.setState({
                searchResults: data
            })
        }
        else
        {
            this.setState({
                searchResults: null
            })
        }
    }

    clearIputFields () {
        const NEWinputFieldsValue = this.state.inputFieldsValue

        for (let prop in NEWinputFieldsValue)  {
            if(!NEWinputFieldsValue.hasOwnProperty(prop)) continue;

            NEWinputFieldsValue[prop] = ''
        }

        this.setState({
            startDate: new Date().toISOString(),
            endDate: new Date().toISOString(),

            inputFieldsValue : NEWinputFieldsValue,
            selectedSearch: '',
            searchResults: null,
            selectedData: {},
            editTournament: false,
            idToUpdate: '',
            showDelete: false,

            groupsDone: false,
            groupsData: {},

            teamsToFinals: 0,

            editGroup: {
                reset: true
            },

            finalsDone: false,
            finalsData: {},

            editFinals: {
                reset: true
            },

            playerSearchResults: [],
            playerList: [],
            playerListLabels: [],
            dataListOptionPlayer: [],

            teamSearchResults: [],
            teamList: [],
            teamListLabels: [],
            dataListOptionTeam: [],

            matchesSearchResults: [],
            matchesList: [],
            matchesListLabels: [],
            dataListOptionMaches: [],

            relatedTournamentsSearchResults: [],
            relatedTournamentsList: [],
            relatedTournamentsListLabels: [],
            dataListOptionRelatedTournaments: []
        })

    }

    handelAdd (e) {

        e.preventDefault()
        let {
                addTournamentSchema
            ,   editGroup
            ,   editFinals
        } = this.state

        const {
                idToUpdate
            ,   editTournament
            ,   playerList
            ,   teamList
            ,   selectedData

            ,   groupsDone
            ,   finalsDone

            ,   groupsData

            ,   teamsToFinals
            ,   finalsData

            ,   startDate
            ,   endDate

        } = this.state

        const validation = InputValidation( addTournamentSchema, this.refs, false, true )

        let inputQuery = addTournamentQ,
        queryType = 'addTournament'

        addTournamentSchema = validation.schema;

        //add group and final fromat/schema
        if ( !groupsDone || !finalsDone )
        {
            validation.hasError = true
            if (  !groupsDone )
            {
                editGroup = {}
                editGroup.error = 'Please complete the Group Stage Tournament Structure'
            }

            if (  !finalsDone )
            {
                editFinals = {}
                editFinals.error = 'Please complete the Final Stage Tournament Structure'
            }
        }

        if ( !startDate || !endDate  )
        {
            validation.hasError = true
            //error handeling dates.
        }

        this.setState({
            editGroup,
            editFinals,
            addTournamentSchema
        })

        if ( !validation.hasError )
        {
            const inputItems = validation.inputItems

            inputItems.startDate = startDate
            inputItems.endDate = endDate

            if ( playerList.length !== 0 )
            {
                inputItems.players =  playerList
            }

            if ( teamList.length !== 0 )
            {
                inputItems.teams =  teamList
            }

            if ( editTournament )
            {
               inputItems.Id = idToUpdate;
               //inputItems.leagueid = selectedData.leagueid;
               if ( selectedData.league_tier !== null )
               {
                    inputItems.league_tier = selectedData.league_tier;
               }
               else
               {
                    inputItems.league_tier = ''
               }
               inputQuery = updateTournamentQ
               queryType = 'updateTournament'
            }
            inputItems.format = {
                teamAmount: inputItems.nrTeams,
                groupStage: {},
                finalStage: {}
            }

            inputItems.format.groupStage = groupsData
            inputItems.format.finalStage = finalsData

            this.props.submitForm( inputQuery, inputItems)
            .then(result => {
                const data = JSON.parse(result.body).data[queryType]
                this.showEdit( [data] )
            })
            .catch(error => {
                console.log('handelAdd error >', error)
            })
        }
    }

    deleteThisTournament()
    {
        const { idToUpdate } = this.state
        let confirmDelete = confirm("Are you sure you want to delete this Team ("+ idToUpdate +")");

        if ( confirmDelete )
        {
            this.props.submitForm( deleteTournament, { Id: idToUpdate })
            .then(result => {
                this.clearIputFields()
            })
            .catch(error => {
                console.log('handelAdd error >', error)
            })
        }
    }

    showEdit(data)
    {
        console.log( "showEdit" )
        this.clearIputFields()
        this.ShowSearchResults( data )
    }

    toggleAptDisplay ()
    {
        this.props.handleToggle('AddTournament')
    }

    onChangeDatalist (event)
    {
        const value = event.target.value,
        listName = event.target.getAttribute('list')
        let { inputFieldsValue } = this.state
        inputFieldsValue[listName] = value
        this.setState({
            inputFieldsValue
        })


        if (value.length > 1 && listName === 'player' )
        {
            this.props.searchForm( findPlayerQ, {QueryStr: value } )
            .then(result => {
                const data  = JSON.parse(result.body).data.findPlayer
                if ( data.length !== 0 )
                {
                    let dataListOptionPlayer = []
                    data.map(
                        (data, i) =>{
                           dataListOptionPlayer.push( data.nickName )
                       }
                    )
                    this.setState({
                        playerSearchResults: data,
                        dataListOptionPlayer
                    })
                }
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else if (value.length > 1 && listName === 'team' )
        {
            this.props.searchForm( searchTeamQ, {QueryStr: value } )
            .then(result => {
                const data  = JSON.parse(result.body).data.searchTeam
                if ( data.length !== 0 )
                {
                    let dataListOptionTeam = []
                    data.map(
                        (data, i) =>{
                           dataListOptionTeam.push( data.name )
                       }
                    )
                    this.setState({
                        teamSearchResults: data,
                        dataListOptionTeam
                    })
                }
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else if (value.length > 1 && listName === 'relatedTournaments' )
        {
            this.props.searchForm( findTournamentQ, {QueryStr: value } )
            .then(result => {
                const data  = JSON.parse(result.body).data.findTournament
                if ( data.length !== 0 )
                {
                    let dataListOptionRelatedTournaments = []
                    data.map(
                        (data, i) =>{
                           dataListOptionRelatedTournaments.push( data.name )
                       }
                    )
                    this.setState({
                        relatedTournamentsSearchResults: data,
                        dataListOptionRelatedTournaments
                    })
                }
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else
        {
            this.setState({
                playerSearchResults: [],
                dataListOptionPlayer: [],
                teamSearchResults: [],
                dataListOptionTeam: [],
                matchesSearchResults: [],
                dataListOptionMaches: [],
                relatedTournamentsSearchResults: [],
                dataListOptionRelatedTournaments: []
            })
        }
    }

    dlOptionSelected (inputValue, listName)
    {
        //add more switches
        if (listName === 'player' )
        {
            const { playerSearchResults } = this.state
            let {inputFieldsValue, playerList, playerListLabels} = this.state
            inputFieldsValue.player = ''

            playerSearchResults.map(
                (valueD, i) =>{
                    if (valueD.nickName ===  inputValue ){
                        playerList.push({ accountId: valueD.accountId })
                        playerListLabels.push({ id: valueD.accountId, name: valueD.nickName  } )
                    }
                }
            )

            this.setState({
                playerSearchResults: null,
                dataListOptionPlayer: [],
                inputFieldsValue,
                playerList,
                playerListLabels
            })
        }

        if (listName === 'team' )
        {
            const { teamSearchResults } = this.state
            let {inputFieldsValue, teamList, teamListLabels} = this.state
            inputFieldsValue.team = ''

            teamSearchResults.map(
                (valueD, i) =>{
                    if (valueD.name ===  inputValue ){
                        teamList.push({ steamId: valueD.steamId })
                        teamListLabels.push({ id: valueD.steamId, name: valueD.name  } )
                    }
                }
            )

            this.setState({
                teamSearchResults: null,
                dataListOptionTeam: [],
                inputFieldsValue,
                teamList,
                teamListLabels
            })
        }
    }

    returnGroupInfo( groupsData )
    {

        if ( groupsData === 'reset' )
        {
            this.setState({
                editGroup: null
            })
        }
        else if ( groupsData === 'inEdit' )
        {
            this.setState({
                groupsDone: false,
                editGroup: null
            })
        }
        else
        {
            const { inputFieldsValue } = this.state
            const { eliminatedFromGroups } = groupsData
            let teamsToFinals = 0

            if ( inputFieldsValue.nrTeams !== undefined  &&  eliminatedFromGroups !== undefined  )
            {
                teamsToFinals = parseInt ( inputFieldsValue.nrTeams ) -  eliminatedFromGroups
            }
            else
            {
                teamsToFinals = parseInt ( inputFieldsValue.nrTeams )
            }

            this.setState({
                groupsData,
                teamsToFinals,
                groupsDone: true,
                editGroup: null
            })
        }
    }

    returnFinalInfo( finalsData )
    {
        if ( finalsData === 'reset' )
        {
            this.setState({
                editFinals: null
            })
        }
        else if ( finalsData === 'inEdit' )
        {
            this.setState({
                finalsDone: false,
                editFinals: null
            })
        }
        else
        {
            this.setState({
                finalsData,
                finalsDone: true,
                editFinals: null
            })
        }
    }

    startDateChange (value, formattedValue)
    {
        this.setState({
            startDate: value
        })
    }

    endDateChange (value, formattedValue)
    {
        this.setState({
            endDate: value
        })
    }


    render() {

        const displayAptBody = {
            display : this.props.bodyVisible.AddTournament ? 'block' : 'none'
        }

        const {
            isActiveTeam
            , startDate
            , endDate
            , addTournamentSchema
            , searchResults
            , selectedSearch
            , inputFieldsValue
            , editTournament
            , showDelete
            , forcePoly

            , editGroup
            , editFinals

            , groupsDone
            , teamsToFinals

            , playerList
            , playerListLabels
            , dataListOptionPlayer

            , teamList
            , teamListLabels
            , dataListOptionTeam

            , matchesList
            , matchesListLabels
            , dataListOptionMaches

            , relatedTournamentsList
            , relatedTournamentsLabels
            , dataListOptionRelatedTournaments

        } = this.state

        const submitBtnText = editTournament ? 'Edit Tournament' : 'Add Tournament',
        deleteBtnStyle = showDelete ? { display: 'block' } : { display: 'none' }

        let searchPanel = '',
        playersInVal = inputFieldsValue.players ?  inputFieldsValue.players : '',
        playerListJSX = '',

        teamsInVal = inputFieldsValue.teams ?  inputFieldsValue.teams : '',
        teamListJSX = '',

        matchesInVal = inputFieldsValue.matches ?  inputFieldsValue.matches : '',
        matchesListJSX = '',

        relatedTournamentsInVal = inputFieldsValue.relatedTournaments ?  inputFieldsValue.relatedTournaments : '',
        relatedTournamentsListJSX = ''

        playerListJSX = Object.keys(playerListLabels).map(
            (key) =>{
                return MakeLabelListJSX( playerListLabels[key], key, this.removeLabel, 'player' )
            }
        )

        teamListJSX = Object.keys(teamListLabels).map(
            (key) =>{
                return MakeLabelListJSX( teamListLabels[key], key, this.removeLabel, 'team'  )
            }
        )

        matchesListJSX = Object.keys(matchesListLabels).map(
            (key) =>{
                return MakeLabelListJSX( matchesListLabels[key], key, this.removeLabel, 'matches' )
            }
        )

        relatedTournamentsListJSX = Object.keys(relatedTournamentsLabels).map(
            (key) =>{
                return MakeLabelListJSX( relatedTournamentsLabels[key], key, this.removeLabel, 'relatedTournaments' )
            }
        )

        if (searchResults !== null )
        {
            searchPanel = ( ShowSearch( searchResults, this.onClickSearchRow, selectedSearch, this.editSearchRow, 'tournament' ) )
        }

        return (
            <div className="addTournament">
                <div className="TournamentInput">
                    <div className="panel panel-primary">
                        <div className="panel-heading apt-addheading" onClick={ this.toggleAptDisplay } >
                        <span className="glyphicon glyphicon-plus"></span> Add Tournament</div>
                        <div className="panel-body" style={ displayAptBody } >
                            <form className="add-appointment form-horizontal" onSubmit={ this.handelAdd } >
                                <input type="hidden" id="id" value={ selectedSearch } />

                                <div className="form-group">
                                    <label className="col-sm-2 control-label">Start Date</label>
                                    <div className="col-sm-10 container">
                                        <div className="col-sm-3">
                                            <DatePicker
                                                id="startDate"
                                                value={ startDate }
                                                onChange={this.startDateChange}
                                                dateFormat="DD/MM/YYYY"
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-sm-2 control-label">End Date</label>
                                    <div className="col-sm-10 container">
                                        <div className="col-sm-3">
                                            <DatePicker
                                                id="endDate"
                                                value={ endDate }
                                                onChange={this.endDateChange}
                                                dateFormat="DD/MM/YYYY"
                                            />
                                        </div>
                                    </div>
                                </div>

                                {addTournamentSchema.map(
                                    (data, i) =>
                                        MakeForm(data, i, this.onChangeHandeler, inputFieldsValue )
                                )}
                                <div className="well">Tournament Structure</div>

                                <div className="form-group">
                                    <label className="col-sm-2 control-label">Group Stage</label>
                                    <div className="col-sm-10 container">
                                        <GroupstageScheme
                                            teamAmount={ inputFieldsValue.nrTeams }
                                            returnGroup={ this.returnGroupInfo }
                                            editGroup={ editGroup }
                                        />
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-sm-2 control-label">Final Format</label>
                                    <div className="col-sm-10 container">
                                        <FinalsScheme
                                            teamsToFinals={ teamsToFinals }
                                            groupsDone={ groupsDone }
                                            returnFinals={ this.returnFinalInfo }
                                            editFinals={ editFinals }
                                        />
                                    </div>
                                </div>

                                <div className="form-group dataList-group">
                                    <label className="col-sm-2 control-label" htmlFor="player">Player</label>
                                    <div className="col-sm-10">

                                        <ReactDataList
                                            list="player"
                                            options={ dataListOptionPlayer }
                                            className="form-control"
                                            forcePoly={ true }
                                            includeLayoutStyle={ false }
                                            autoPosition={ false }
                                            initialFilter={ playersInVal }
                                            onInputChange={ this.onChangeDatalist }
                                            onOptionSelected={ this.dlOptionSelected }
                                        />

                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-sm-2 control-label" htmlFor="players">Players List</label>
                                    <div className="col-sm-10 container">
                                        { playerListJSX }
                                    </div>
                                </div>

                                <div className="form-group dataList-group">
                                    <label className="col-sm-2 control-label" htmlFor="team">Team</label>
                                    <div className="col-sm-10">
                                        <ReactDataList
                                            list="team"
                                            options={ dataListOptionTeam }
                                            className="form-control"
                                            forcePoly={ true }
                                            includeLayoutStyle={ false }
                                            autoPosition={ false }
                                            initialFilter={ teamsInVal }
                                            onInputChange={ this.onChangeDatalist }
                                            onOptionSelected={ this.dlOptionSelected }
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-sm-2 control-label" htmlFor="teamsL">Teams List</label>
                                    <div className="col-sm-10 container">
                                        { teamListJSX }
                                    </div>
                                </div>


                                <div className="form-group dataList-group">
                                    <label className="col-sm-2 control-label" htmlFor="matches">Matches</label>
                                    <div className="col-sm-10">
                                        <ReactDataList
                                            list="matches"
                                            options={ dataListOptionMaches }
                                            className="form-control"
                                            forcePoly={ true }
                                            includeLayoutStyle={ false }
                                            autoPosition={ false }
                                            initialFilter={ matchesInVal }
                                            onInputChange={ this.onChangeDatalist }
                                            onOptionSelected={ this.dlOptionSelected }
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-sm-2 control-label" htmlFor="matchesL">Matches List</label>
                                    <div className="col-sm-10 container">
                                        { matchesListJSX }
                                    </div>
                                </div>

                                <div className="form-group dataList-group">
                                    <label className="col-sm-2 control-label" htmlFor="relatedTournaments">Related Tournaments</label>
                                    <div className="col-sm-10">

                                        <ReactDataList
                                            list="relatedTournaments"
                                            options={ dataListOptionRelatedTournaments }
                                            className="form-control"
                                            forcePoly={ true }
                                            includeLayoutStyle={ false }
                                            autoPosition={ false }
                                            initialFilter={ relatedTournamentsInVal }
                                            onInputChange={ this.onChangeDatalist }
                                            onOptionSelected={ this.dlOptionSelected }
                                        />

                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-sm-2 control-label" htmlFor="relatedTournamentsL">Related Tournaments List</label>
                                    <div className="col-sm-10 container">
                                        { relatedTournamentsListJSX }
                                    </div>
                                </div>

                                <div className="form-group">
                                    <div className="col-sm-offset-2 col-sm-10">
                                        <div className="btn-group pull-left" role="group" aria-label="...">
                                            <button type="button" onClick={ this.showAll } className="btn btn-info">Show All</button>
                                        </div>
                                        <div className="btn-group pull-right" role="group" aria-label="...">
                                            <button type="button" style={ deleteBtnStyle } onClick={ this.deleteThisTournament } className="btn btn-danger">Delete Tournament</button>
                                            <button type="button" onClick={ this.clearIputFields } className="btn btn-warning">Clear Input</button>
                                            <button type="submit" className="btn btn-primary">{ submitBtnText }</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                { searchPanel }
            </div>
        )
    }
}