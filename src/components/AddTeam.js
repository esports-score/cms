import React, { Component } from 'react'
import ReactDataList from 'react-datalist'
import { addTeamSchema, searchTeamQ, addTeamQ, updateTeamQ, teamQ, deleteTeam } from '../schemas/addTeam'
import { findPlayerQ } from '../schemas/addPlayer'
import MakeForm from './MakeForm'
import { MakeDorpDownListJSX, MakeLabelListJSX } from './Dropdown_func'
import ShowSearch from './ShowSearch'

import InputValidation from './InputValidation'


export default class AddTeam extends Component {

    constructor( props ) {
        super(props);
        this.state = {
            editTeam: false,
            searchResults: null,
            addTeamSchema,
            inputFieldsValue: {},
            idToUpdate: '',
            selectedSearch: '',
            selectedData: {},
            playerSearchResults: [],
            playerList: [],
            playerListLabels: [],
            showDelete: false,
            dataListOptions: []
        };
        this.toggleAptDisplay = this.toggleAptDisplay.bind(this)
        this.onChangeHandeler = this.onChangeHandeler.bind(this)
        this.handelAdd = this.handelAdd.bind(this)
        this.onClickSearchRow  = this.onClickSearchRow.bind(this)
        this.editSearchRow  = this.editSearchRow.bind(this)
        this.clearIputFields  = this.clearIputFields.bind(this)
        this.removeLabel  = this.removeLabel.bind(this)
        this.showAll  = this.showAll.bind(this)
        this.deleteThisTeam  = this.deleteThisTeam.bind(this)
        this.dlOptionSelected  = this.dlOptionSelected.bind(this)
        this.onChangeDatalist  = this.onChangeDatalist.bind(this)
    }

    onChangeHandeler ( event ) {

        const { addTeamSchema, playerSearchResults, editTeam, inputFieldsValue } = this.state
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = parseInt( target.name )
        const inputName = target.id

        const searchable = addTeamSchema[name].searchable

        inputFieldsValue[inputName] = value
        const validationSearch = InputValidation( addTeamSchema, this.refs, false, false )
        const inputItems = validationSearch.inputItems

        this.setState({
            addTeamSchema,
            inputFieldsValue
        })

        if ( Object.keys(inputItems).length >= 1 && searchable && !editTeam && value.length > 1 ){
            this.props.searchForm( searchTeamQ, inputItems )
            .then(result => {
                const data = JSON.parse(result.body).data.searchTeam
                this.ShowSearchResults( data )
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else if ( Object.keys(inputItems).length === 0 )
        {
            this.setState({
                searchResults: null
            })
        }
    }

    removeLabel (event)
    {
        const target = event.target
        const name = event.target.getAttribute('data-id')
        const type = event.target.getAttribute('data-name')

        let { playerList, playerListLabels } = this.state

        delete playerList[name]
        delete playerListLabels[name]

        this.setState({
            playerList,
            playerListLabels
        })

    }

    onClickSearchRow (selectedData, isSelected, event)
    {
        if ( this.state.selectedSearch !== selectedData.id || this.state.selectedData !== selectedData )
        {
            this.setState({
                selectedSearch: selectedData.id,
                selectedData: selectedData
            })
        }
    }

    editSearchRow( )
    {
        const { players } = this.state.selectedData
        let playerList = [],
        playerListLabels = []
        for (var i = players.length - 1; i >= 0; i--) {
            playerList[i] = { accountId : players[i].accountId }
            playerListLabels[i] = {
                id : players[i].accountId,
                name:  players[i].nickName
            }
        }
        this.setState({
            inputFieldsValue: this.state.selectedData,
            editTeam: true,
            searchResults: null,
            idToUpdate: this.state.selectedSearch,
            playerList,
            playerListLabels,
            showDelete: true
        })
    }

    showAll ( )
    {
        this.props.searchForm( teamQ )
        .then(result => {
            console.log( 'result >', result )
            const data = JSON.parse(result.body).data.team
            console.log( 'data >', data )
            this.ShowSearchResults( data )
        })
        .catch(error => {
            console.log('onChangeHandeler error >', error)
        })
    }

    ShowSearchResults (data)
    {
        if (data.length !== 0 && data[0] !== null )
        {
            this.setState({
                searchResults: data
            })

        }
        else
        {
            this.setState({
                searchResults: null
            })
        }
    }

    clearIputFields () {
        const NEWinputFieldsValue = this.state.inputFieldsValue

        for (let prop in NEWinputFieldsValue)  {
            if(!NEWinputFieldsValue.hasOwnProperty(prop)) continue;

            NEWinputFieldsValue[prop] = ''

        }

        this.setState({
            inputFieldsValue : NEWinputFieldsValue,
            selectedSearch: '',
            searchResults: null,
            selectedData: {},
            editTeam: false,
            idToUpdate: '',
            playerSearchResults: [],
            playerList: [],
            playerListLabels: [],
            showDelete: false,
            dataListOptions: []
        })
    }

    handelAdd (e) {
        e.preventDefault()
        let { addTeamSchema } = this.state
        const hasError = false,
        { idToUpdate, editTeam, playerList } = this.state,

        validation = InputValidation( addTeamSchema, this.refs, hasError, true )
        let inputQuery = addTeamQ,
        queryType = 'addTeam'

        addTeamSchema = validation.schema;
        this.setState({
            addTeamSchema
        })

        if ( !validation.hasError && playerList.length !== 0 )
        {
            const inputItems = validation.inputItems
            inputItems.players =  playerList

            if ( editTeam )
            {
               inputItems.Id = idToUpdate;
               inputQuery = updateTeamQ
               queryType = 'updateTeam'
            }

            this.props.submitForm( inputQuery, inputItems)
            .then(result => {
                const data = JSON.parse(result.body).data[queryType]
                this.showEdit( [data] )
            })
            .catch(error => {
                console.log('handelAdd error >', error)
            })
        }
    }

    deleteThisTeam()
    {
        const { idToUpdate } = this.state
        let confirmDelete = confirm("Are you sure you want to delete this Team ("+ idToUpdate +")");

        if ( confirmDelete )
        {
            this.props.submitForm( deleteTeam, { Id: idToUpdate })
            .then(result => {
                this.clearIputFields()
            })
            .catch(error => {
                console.log('handelAdd error >', error)
            })
        }
    }

    showEdit(data)
    {
        this.clearIputFields()
        this.ShowSearchResults( data )
    }

    toggleAptDisplay ()
    {
        this.props.handleToggle('AddTeam')
    }

    onChangeDatalist (event)
    {
        const value = event.target.value
        let { inputFieldsValue } = this.state
        inputFieldsValue.player = value
        this.setState({
            inputFieldsValue
        })
        if (value.length > 1 )
        {
            this.props.searchForm( findPlayerQ, {QueryStr: value } )
            .then(result => {
                const data  = JSON.parse(result.body).data.findPlayer
                if ( data.length !== 0 )
                {
                    let dataListOptions = []
                    data.map(
                        (data, i) =>{
                           dataListOptions.push( data.nickName )
                       }
                    )
                    this.setState({
                        playerSearchResults: data,
                        dataListOptions
                    })
                }
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else
        {
            this.setState({
                playerSearchResults: [],
                dataListOptions: []
            })
        }
    }

    dlOptionSelected (playerNick)
    {
        const { playerSearchResults } = this.state
        let {inputFieldsValue, playerList, playerListLabels} = this.state
        inputFieldsValue.player = ''

        playerSearchResults.map(
            (valueD, i) =>{
                if (valueD.nickName ===  playerNick ){
                    playerList.push({ accountId: valueD.accountId })
                    playerListLabels.push({ id: valueD.accountId, name: valueD.nickName  } )
                }
            }
        )

        this.setState({
            playerSearchResults: null,
            inputFieldsValue,
            playerList,
            playerListLabels
        })

    }

    render() {

        const displayAptBody = {
            display : this.props.bodyVisible.AddTeam ? 'block' : 'none'
        }

        const {
            isActiveTeam
            , addTeamSchema
            , searchResults
            , selectedSearch
            , inputFieldsValue
            , editTeam
            , playerSearchResults
            , playerList
            , playerListLabels
            , showDelete
            , forcePoly
            , dataListOptions
        } = this.state

        const submitBtnText = editTeam ? 'Edit Team' : 'Add Team',
        deleteBtnStyle = showDelete ? { display: 'block' } : { display: 'none' }

        let searchPanel = '',
        playersInVal = inputFieldsValue.players ?  inputFieldsValue.players : '',
        playesSearchList = '',
        playerListJSX = ''

        playerListJSX = Object.keys(playerListLabels).map(
            (key) =>{
                return MakeLabelListJSX( playerListLabels[key], key, this.removeLabel, 'player' )
            }
        )

        if (searchResults !== null )
        {
            searchPanel = ( ShowSearch( searchResults, this.onClickSearchRow, selectedSearch, this.editSearchRow, 'team' ) )
        }

        return (
            <div className="addTeam">
                <div className="TeamInput">
                    <div className="panel panel-primary">
                        <div className="panel-heading apt-addheading" onClick={ this.toggleAptDisplay } >
                        <span className="glyphicon glyphicon-plus"></span> Add Team</div>
                        <div className="panel-body" style={ displayAptBody } >
                            <form className="add-appointment form-horizontal" onSubmit={ this.handelAdd } >
                                <input type="hidden" id="id" value={ selectedSearch } />
                                {addTeamSchema.map(
                                    (data, i) =>
                                        MakeForm(data, i, this.onChangeHandeler, inputFieldsValue )
                                )}
                                <div className="form-group dataList-group">
                                    <label className="col-sm-2 control-label" htmlFor="player">Player</label>
                                    <div className="col-sm-10">

                                        <ReactDataList
                                            list="player"
                                            options={dataListOptions}
                                            className="form-control"
                                            forcePoly={true}
                                            includeLayoutStyle={false}
                                            autoPosition={false}
                                            initialFilter={inputFieldsValue.player}
                                            onInputChange={this.onChangeDatalist}
                                            onOptionSelected={this.dlOptionSelected}
                                        />

                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-sm-2 control-label" htmlFor="players">Players List</label>
                                    <div className="col-sm-10 container">
                                        { playerListJSX }
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-offset-2 col-sm-10">
                                        <div className="btn-group pull-left" role="group" aria-label="...">
                                            <button type="button" onClick={ this.showAll } className="btn btn-info">Show All</button>
                                        </div>
                                        <div className="btn-group pull-right" role="group" aria-label="...">
                                            <button type="button" style={ deleteBtnStyle } onClick={ this.deleteThisTeam } className="btn btn-danger">Delete Team</button>
                                            <button type="button" onClick={ this.clearIputFields } className="btn btn-warning">Clear Input</button>
                                            <button type="submit" className="btn btn-primary">{ submitBtnText }</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                { searchPanel }
            </div>
        )
    }
}