import React from 'react'


const __isRequired = (required, error, placeholder, name)  => {
    if (required)
    {
        return ( <small className="help-block" data-fv-validator="notEmpty" data-fv-for={ name } data-fv-result="NOT_VALIDATED" style={{display: error.empty ? 'block' : 'none' }} >The { placeholder } is required</small>)
    }
}

const MakeForm = (data, key, onChange, inputFields) => {
    const { placeholder, name, ref, error, required, searchable } = data
    const value = typeof inputFields[name] === 'string' ? inputFields[name] : ''

    return (
        <div className="form-group" key={ key } >
            <label className="col-sm-2 control-label" htmlFor={ name }>{ placeholder }</label>
            <div className="col-sm-10">
                <input type="text" className="form-control" onChange={ onChange } id={ name } name={ key } ref={ ref } placeholder={ placeholder } value={ value } />
                <i className="form-control-feedback" data-fv-icon-for={ name } style={{display: 'none'}}></i>
                { __isRequired(required, error, placeholder, name) }
                <small className="help-block" data-fv-validator="regexp" data-fv-for={ name } data-fv-result="NOT_VALIDATED" style={{display: error.regexp ? 'block' : 'none' }}>The { placeholder } can only consist of alphabetical, number, dot and underscore</small>
            </div>
        </div>
    )
}

export default MakeForm