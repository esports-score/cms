import React, { Component } from 'react'
import { ButtonToolbar, SplitButton, MenuItem } from 'react-bootstrap'

export default class GroupstageScheme extends Component {

    constructor( props ) {
        super(props);
        this.state = {
            step: 1,
            groupStage: false,
            errorMsg: "",
            DDgroupAmount: "NONE",
            DDgroupStyle: "NONE",
            DDgroupMatchStyle: "NONE",
            DDgroupElim: 0,
            stepAnswered: 0,
            forwardBtnStyle : {
                display: "none"
            }
        }
        this.stepsJSX = this.stepsJSX.bind(this)
        this.onClickYNbtn = this.onClickYNbtn.bind(this)
        this.onClickStepBackFwrd = this.onClickStepBackFwrd.bind(this)
        this.selectDDgroup = this.selectDDgroup.bind(this)
        this.selectDDgroupStyle = this.selectDDgroupStyle.bind(this)
    }

    shouldComponentUpdate(nextProps, nextState) {

        if (this.props.teamAmount !== nextProps.teamAmount)
        {
            return true;
        }

        if ( nextProps.editGroup !== null &&  this.props.editGroup !== nextProps.editGroup )
        {
            return true;
        }

        if (this.state.step !== nextState.step)
        {
            console.log("this.state.step !== nextState.step" , this.state.step !== nextState.step)
            return true;
        }

        if (this.state.groupStage !== nextState.groupStage)
        {
            console.log("this.state.groupStage !== nextState.groupStage" , this.state.groupStage !== nextState.groupStage)
            return true;
        }

        if (this.state.errorMsg !== nextState.errorMsg)
        {
            console.log("this.state.errorMsg !== nextState.errorMsg" , this.state.errorMsg !== nextState.errorMsg)
            return true;
        }

        if (this.state.DDgroupAmount !== nextState.DDgroupAmount)
        {
            console.log("this.state.DDgroupAmount !== nextState.DDgroupAmount" , this.state.DDgroupAmount !== nextState.DDgroupAmount)
            return true;
        }

        if (this.state.DDgroupStyle !== nextState.DDgroupStyle)
        {
            console.log("this.state.DDgroupStyle !== nextState.DDgroupStyle" , this.state.DDgroupStyle !== nextState.DDgroupStyle)
            return true;
        }

        if (this.state.DDgroupMatchStyle !== nextState.DDgroupMatchStyle)
        {
            console.log("this.state.DDgroupMatchStyle !== nextState.DDgroupMatchStyle" , this.state.DDgroupMatchStyle !== nextState.DDgroupMatchStyle)
            return true;
        }

        // if (this.state.DDgroupElim !== nextState.DDgroupElim)
        // {
        //     console.log("this.state.DDgroupElim !== nextState.DDgroupElim" , this.state.DDgroupElim !== nextState.DDgroupElim)
        //     return true;
        // }

        if (this.state.stepAnswered !== nextState.stepAnswered)
        {
            console.log("this.state.stepAnswered !== nextState.stepAnswered" , this.state.stepAnswered !== nextState.stepAnswered)
            return true;
        }

        if (this.state.forwardBtnStyle !== nextState.forwardBtnStyle)
        {
            console.log("this.state.forwardBtnStyle !== nextState.forwardBtnStyle" , this.state.forwardBtnStyle !== nextState.forwardBtnStyle)
            return true;
        }

        return false;
    }

    posibleGroups(dividend,divisor)
    {
        var result = 0;
        while(dividend >= divisor){
            dividend -= divisor;
            result++;
        }

        return result;
    }

    stepsJSX ()
    {
        const {
            onClickYNbtn,
            selectDDgroup,
            posibleGroups,
            selectDDgroupStyle
        } = this

        const {
            step,
            groupStage,
            DDgroupAmount,
            DDgroupElim,
            DDgroupStyle,
            DDgroupMatchStyle
        } = this.state

        const {
            teamAmount,
            editGroup
        } = this.props

        if ( step === 1 )
        {
            return(
                <div className="col-sm-10">
                    <button className="btn btnYN success" data-val="GroupYes" onClick={ onClickYNbtn }><span data-val="GroupYes" className="glyphicon glyphicon-ok success"></span></button>
                    <button className="btn btnYN danger" data-val="GroupNo" onClick={ onClickYNbtn }><span data-val="GroupNo" className="glyphicon glyphicon-remove danger"></span></button>
                </div>
            )
        }
        else if ( step === 2 && groupStage )
        {
            let posibleGroupsLoop = posibleGroups( teamAmount , 4 ) - 2

            let options = []
            for (let i = 0; i < posibleGroupsLoop; i++) {
                let x = i + 3
                const num = teamAmount / x
                if ( num % 1 == 0 )
                {
                    options.push( <option key={`option-key${i}-${x}`} value={ x }>{ x }</option> )
                }
            }

            return(
                <div className="col-sm-10">
                    <div className="form-group">
                        <select onChange={ selectDDgroup  } value={ DDgroupAmount } className="form-control" id="groupAmount">
                            <option defaultValue value="NONE">Amount of Groups</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            {
                                options.map(
                                    (data, i) =>{
                                        return ( data )
                                    }
                                )
                            }
                        </select>
                    </div>
                </div>
            )
        }
        else if ( step === 2 && !groupStage  )
        {
            //this.props.returnGroup( groupStage )
            return(
                <div className="col-sm-10">
                    <b>There is no Groupstage</b>
                </div>
            )
        }
        else if ( step === 3 )
        {
            return(
                <div className="col-sm-10 form-group">
                    <div className="col-sm-5">
                        <select onChange={ selectDDgroupStyle  } value={ DDgroupStyle } className="form-control" id="DDgroupStyle">
                            <option defaultValue value="Group structure">Group structure</option>
                            <option value="RoundRobin">Round Robin</option>
                            <option value="GSL">GSL</option>
                            <option value="Swiss">Swiss</option>
                        </select>
                    </div>
                    <div className="col-sm-5">
                        <select onChange={ selectDDgroupStyle  } value={ DDgroupMatchStyle } className="form-control" id="DDgroupMatchStyle">
                            <option defaultValue value="Beste Of">Beste Of</option>
                            <option value="BO1">Beste Of One</option>
                            <option value="BO2">Beste Of Two</option>
                            <option value="BO3">Beste Of Three</option>
                        </select>
                    </div>
                </div>

            )
        }
        else if ( step === 4 )
        {
            return(
                <div className="col-sm-10">
                    <div className="col-sm-3">groups: <b>{ DDgroupAmount }</b></div>
                    <div className="col-sm-3">Teams/group: <b>{ teamAmount / DDgroupAmount }</b></div>
                    <div className="col-sm-6">Structure: <b>{ DDgroupStyle } / { DDgroupMatchStyle }</b></div>
                </div>
            )
        }
    }

    componentDidUpdate(){

        console.log( "componentDidUpdate" )

        let {
            step,
            groupStage,
            DDgroupAmount,
            DDgroupStyle,
            DDgroupMatchStyle,
            DDgroupElim,
            stepAnswered
        } = this.state

        const {
            editGroup,
            teamAmount,
            returnGroup
        } = this.props

        if ( step === 2 && !groupStage )
        {
            returnGroup( { hasGroupStage: groupStage } )
        }
        else if ( step === 4 )
        {
            returnGroup( {
                hasGroupStage: groupStage,
                groupsAmount: DDgroupAmount,
                typeGroup: DDgroupStyle,
                typeGroupMatches: DDgroupMatchStyle,
                eliminatedFromGroups: DDgroupElim
            })
        }
        else if ( stepAnswered === 4 && step < 4 )
        {
            returnGroup( 'inEdit' )
        }

        if ( editGroup !== null && editGroup.reset !== undefined && editGroup.reset )
        {
            this.setState({
                step: 1,
                groupStage: false,
                errorMsg: "",
                DDgroupAmount: "NONE",
                DDgroupStyle: "NONE",
                DDgroupMatchStyle: "NONE",
                DDgroupElim: 0,
                stepAnswered: 0,
                forwardBtnStyle : {
                    display: "none"
                }
            })

            returnGroup( 'reset' )
        }
        else if ( editGroup !== null && typeof editGroup.error === 'string' )
        {
            this.setState({
                errorMsg: editGroup.error
            })
        }
        else if ( editGroup !== null && typeof editGroup.edit === 'object' )
        {
            console.log( "editGroup.edit" )

            groupStage = editGroup.edit.hasGroupStage

            if ( groupStage ){

                const teamElim = ( teamAmount - editGroup.edit.teamsToFinals ) / editGroup.edit.groupsAmount

                DDgroupAmount = editGroup.edit.groupsAmount
                DDgroupStyle = editGroup.edit.typeGroup
                DDgroupMatchStyle = editGroup.edit.typeGroupMatches
                DDgroupElim = teamElim
                stepAnswered = 4
                step = 4
            }
            else
            {
                stepAnswered = 2
                step = 2
            }

            this.setState({
                groupStage,
                DDgroupAmount,
                DDgroupStyle,
                DDgroupMatchStyle,
                DDgroupElim,
                stepAnswered,
                step,
            })

            if ( step === 2 && !groupStage )
            {
                returnGroup( { hasGroupStage: groupStage } )
            }
            else if ( step === 4 )
            {
                returnGroup( {
                    hasGroupStage: groupStage,
                    groupsAmount: DDgroupAmount,
                    typeGroup: DDgroupStyle,
                    typeGroupMatches: DDgroupMatchStyle,
                    eliminatedFromGroups: DDgroupElim
                })
            }
        }
    }

    selectDDgroup ( event )
    {
        let {
            step,
            stepAnswered
        } = this.state

        step++
        stepAnswered = stepAnswered < step ? step : stepAnswered
        this.setState({
            step,
            stepAnswered,
            DDgroupAmount : event.target.value
        })
    }

    selectDDgroupStyle( event )
    {
        const finals = [ 16, 8, 4 ]
        let {
            step,
            DDgroupElim,
            DDgroupStyle,
            DDgroupMatchStyle,
            DDgroupAmount,
            stepAnswered
        } = this.state

        const {
            teamAmount
        } = this.props

        const id = event.target.id

        if ( id === 'DDgroupStyle')
        {
            DDgroupStyle = event.target.value
        }
        else if ( id === 'DDgroupMatchStyle' )
        {
            DDgroupMatchStyle = event.target.value
        }

        if ( DDgroupStyle !== "NONE" && DDgroupMatchStyle !== "NONE" )
        {
            step++
            stepAnswered = stepAnswered < step ? step : stepAnswered
        }

        let teamAmountInt = parseInt( teamAmount )

        for (let key in finals) {
            if ( finals.hasOwnProperty(key) ){

                if ( teamAmountInt > finals[key] )
                {
                    DDgroupElim = ( teamAmountInt - finals[key] )
                    break;
                }
            }
        }

        this.setState({
            step,
            DDgroupElim,
            stepAnswered,
            DDgroupStyle,
            DDgroupMatchStyle
        })
    }

    onClickYNbtn(event)
    {
        event.preventDefault()
        const idName = event.target.getAttribute('data-val')

        let {
            step,
            groupStage,
            errorMsg,
            stepAnswered
        } = this.state

        const {
            teamAmount
        } = this.props

        if ( idName === 'GroupNo' )
        {
            groupStage = false
            errorMsg = ""
            step++
        }
        else if ( teamAmount === undefined ||  teamAmount === "" )
        {
            errorMsg = "Please fill in the amount of teams first!";
        }
        else
        {
            groupStage = true
            errorMsg = ""
            step++
        }
        stepAnswered = stepAnswered < step ? step : stepAnswered
        this.setState({
            step,
            groupStage,
            stepAnswered,
            errorMsg
        })
    }

    onClickStepBackFwrd( event )
    {
        const idName = event.target.getAttribute('data-id')
        event.preventDefault()

        let {
            step,
            forwardBtnStyle,
            stepAnswered
        } = this.state

        if ( idName === 'backBtn' )
        {
            step--
            forwardBtnStyle = {
                display: "inline-block"
            }
        }
        else if ( idName === 'fwrBtn' )
        {
            step++

            if ( step >=  stepAnswered )
            {
                forwardBtnStyle = {
                    display: "none"
                }
            }
        }

        this.setState({
            step,
            forwardBtnStyle
        })
    }

    render() {

        const {
            onClickYNbtn,
            onClickStepBackFwrd,
            stepsJSX
        } = this

        const stepsJSXrtn = stepsJSX()
        const {
            step,
            errorMsg,
            forwardBtnStyle
        } = this.state

        let backBtnStyle = {
            display: "none"
        }

        if ( step > 1 )
        {
            backBtnStyle =  {
                display: "inline-block"
            }
        }

        return (
            <div id="GroupstageDiv">
                { stepsJSXrtn }
                <div className="col-sm-2">
                    <button style={ backBtnStyle } data-id="backBtn" className="btn btnYN btn-danger" onClick={ onClickStepBackFwrd }>
                        <span data-id="backBtn" className="glyphicon glyphicon-step-backward"></span>
                    </button>
                    <button style={ forwardBtnStyle } data-id="fwrBtn" className="btn btnYN btn-info" onClick={ onClickStepBackFwrd }>
                        <span data-id="fwrBtn" className="glyphicon glyphicon-step-forward"></span>
                    </button>
                </div>
                <small className="help-block col-sm-12">{ errorMsg }</small>
            </div>
        )
    }
}