import React, { Component } from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import { addPlayerSchema, searchPlayerQ, findPlayerQ, addPlayerQ, updatePlayerQ, playersQ, deletePlayer } from '../schemas/addPlayer'
import MakeForm from './MakeForm'
import ShowSearch from './ShowSearch'


import InputValidation from './InputValidation'


export default class AddPlayer extends Component
{
    constructor( props )
    {
        super(props);
        this.state = {
            isActivePlayer: 'True',
            editPlayer: false,
            searchResults: null,
            addPlayerSchema,
            inputFieldsValue: {},
            idToUpdate: '',
            selectedSearch: '',
            selectedData: {},
            showDelete: false
        };
        this.changeRadio = this.changeRadio.bind(this)
        this.toggleAptDisplay = this.toggleAptDisplay.bind(this)
        this.onChangeHandeler = this.onChangeHandeler.bind(this)
        this.handelAdd = this.handelAdd.bind(this)
        this.onClickSearchRow  = this.onClickSearchRow.bind(this)
        this.editSearchRow  = this.editSearchRow.bind(this)
        this.clearIputFields  = this.clearIputFields.bind(this)
        this.showAll  = this.showAll.bind(this)
        this.deleteThisPlayer  = this.deleteThisPlayer.bind(this)
    }

    componentWillMount()
    {
        const { addPlayerSchema } = this.state

        let inputFieldsValue = {}
        addPlayerSchema.map(
            (data, i) =>{
                inputFieldsValue[data.name] = '';
            }
        )

        this.setState({
            inputFieldsValue
        })
    }

    changeRadio ( event )
    {
        this.setState({
            isActivePlayer : event.target.value
        })
    }

    onChangeHandeler ( event )
    {
        const NEWaddPlayerSchema = this.state.addPlayerSchema
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = parseInt( target.name )

        const searchable = this.state.addPlayerSchema[name].searchable
        const inputName = target.id
        const inputFields = this.state.inputFieldsValue
        inputFields[inputName] = value
        const tempErroState = this.state.addPlayerSchema
        const validationSearch = InputValidation( tempErroState, this.refs, false, false )
        const inputItems = validationSearch.inputItems

        this.setState({
            addPlayerSchema : NEWaddPlayerSchema,
            inputFieldsValue: inputFields
        })

        if ( Object.keys(inputItems).length >= 1 && searchable && !this.state.editPlayer && value.length > 1 ){
            this.props.searchForm( searchPlayerQ, inputItems )
            .then(result => {
                const data = JSON.parse(result.body).data.searchPlayer
                this.ShowSearchResults( data )
            })
            .catch(error => {
                console.log('onChangeHandeler error >', error)
            })
        }
        else if ( Object.keys(inputItems).length === 0 )
        {
            this.setState({
                searchResults: null
            })
        }
    }

    onClickSearchRow (selectedData, isSelected, event)
    {
        if ( this.state.selectedSearch !== selectedData.id || this.state.selectedData !== selectedData )
        {
            this.setState({
                selectedSearch: selectedData.id,
                selectedData: selectedData
            })
        }
    }

    editSearchRow( )
    {
        const isActivePlayer = this.state.selectedData.active ? 'True' : 'False';
        this.setState({
            isActivePlayer,
            inputFieldsValue: this.state.selectedData,
            editPlayer: true,
            searchResults: null,
            idToUpdate: this.state.selectedSearch,
            showDelete: true
        })
    }

    showAll ( )
    {
        this.props.searchForm( playersQ )
        .then(result => {
            const data = JSON.parse(result.body).data.players
            this.ShowSearchResults( data )
        })
        .catch(error => {
            console.log('onChangeHandeler error >', error)
        })
    }

    ShowSearchResults (data)
    {
        if (data.length > 0)
        {
            this.setState({
                searchResults: data
            })

        }else{
            this.setState({
                searchResults: null
            })
        }
    }

    clearIputFields ()
    {
        const NEWinputFieldsValue = this.state.inputFieldsValue

        for (let prop in NEWinputFieldsValue)  {
            if(!NEWinputFieldsValue.hasOwnProperty(prop)) continue;

            NEWinputFieldsValue[prop] = ''

        }

        this.setState({
            inputFieldsValue : NEWinputFieldsValue,
            selectedSearch: '',
            searchResults: null,
            selectedData: {},
            editPlayer: false,
            idToUpdate: '',
            showDelete: false
        })
    }

    handelAdd (e)
    {
        e.preventDefault()
        const hasError = false,
        isActive = this.state.isActivePlayer === 'True' ? true : false,
        newState = this.state,
        tempErroState = this.state.addPlayerSchema,
        validation = InputValidation( tempErroState, this.refs, hasError, true )
        let inputQuery = addPlayerQ
        let queryType = 'addPlayer'

        newState.addPlayerSchema = validation.schema;
        this.setState(
            newState
        )

        if ( !validation.hasError )
        {
            const inputItems = validation.inputItems
            inputItems['active'] = isActive
            if (this.state.editPlayer)
            {
               inputItems.Id = this.state.idToUpdate;
               inputQuery = updatePlayerQ
               queryType = 'updatePlayer'
            }

            this.props.submitForm( inputQuery, inputItems)
            .then(result => {
                const data = JSON.parse(result.body).data[queryType]
                this.showEdit( [ data ] )
            })
            .catch(error => {
                console.log('handelAdd error >', error)
            })
        }
    }

    showEdit(data)
    {

        this.clearIputFields()
        this.ShowSearchResults(data)
    }

    toggleAptDisplay ()
    {
        this.props.handleToggle('AddPlayer')
    }

    deleteThisPlayer()
    {
        const { idToUpdate } = this.state
        let confirmDelete = confirm("Are you sure you want to delete this Player ("+ idToUpdate +")");

        if ( confirmDelete )
        {
            this.props.submitForm( deletePlayer, { Id: idToUpdate })
            .then(result => {
                this.clearIputFields()
            })
            .catch(error => {
                console.log('handelAdd error >', error)
            })
        }
    }

    render() {

        const displayAptBody = {
            display : this.props.bodyVisible.AddPlayer ? 'block' : 'none'
        }
        const { isActivePlayer, addPlayerSchema, searchResults, selectedSearch, inputFieldsValue, editPlayer, showDelete } = this.state,
        submitBtnText = editPlayer ? 'Edit Player' : 'Add Player',
        deleteBtnStyle = showDelete ? { display: 'block' } : { display: 'none' }

        let searchPanel = ''

        if (searchResults !== null )
        {
            searchPanel = ( ShowSearch( searchResults, this.onClickSearchRow, selectedSearch, this.editSearchRow, 'player' ) )
        }

        return (
            <div className="addPlayer">
                <div className="PlayerInput">
                    <div className="panel panel-primary">
                        <div className="panel-heading apt-addheading" onClick={ this.toggleAptDisplay } >
                        <span className="glyphicon glyphicon-plus"></span> Add Player</div>
                        <div className="panel-body" style={ displayAptBody } >
                            <form className="add-appointment form-horizontal" onSubmit={ this.handelAdd } >
                                <input type="hidden" id="id" value={ selectedSearch } />
                                <div className="form-group" >
                                    <div className="col-sm-2 control-label"><b>Active</b></div>
                                    <div className="col-xs-6 col-sm-4">
                                        <div className="input-group">
                                            <span className="input-group-addon">
                                                <input
                                                    type="radio"
                                                    name="playerIsActive"
                                                    ref="inputPlayerActive"
                                                    id="PlayerActiveTrue"
                                                    value="True"
                                                    onChange={this.changeRadio}
                                                    checked={ isActivePlayer === 'True' }
                                                    aria-label="True"
                                                />
                                            </span>
                                            <label className="form-control" htmlFor="PlayerActiveTrue">True</label>
                                        </div>
                                    </div>

                                    <div className="col-xs-6 col-sm-4">
                                        <div className="input-group">
                                            <span className="input-group-addon">
                                                <input
                                                    type="radio"
                                                    name="playerIsActive"
                                                    ref="inputPlayerActive"
                                                    id="PlayerActiveFalse"
                                                    value="False"
                                                    onChange={this.changeRadio}
                                                    checked={ isActivePlayer === 'False' }
                                                    aria-label="False"
                                                />
                                            </span>
                                            <label className="form-control" htmlFor="PlayerActiveFalse">False</label>
                                        </div>
                                    </div>
                                </div>
                                {addPlayerSchema.map(
                                    (data, i) =>
                                        MakeForm(data, i, this.onChangeHandeler, inputFieldsValue )
                                )}
                                <div className="form-group">
                                    <div className="col-sm-offset-2 col-sm-10">
                                        <div className="btn-group pull-left" role="group" aria-label="...">
                                            <button type="button" onClick={ this.showAll } className="btn btn-info">Show All</button>
                                        </div>
                                        <div className="btn-group pull-right" role="group" aria-label="...">
                                            <button type="button" style={ deleteBtnStyle } onClick={ this.deleteThisPlayer } className="btn btn-danger">Delete Player</button>
                                            <button type="button" onClick={ this.clearIputFields } className="btn btn-warning">Clear Input</button>
                                            <button type="submit" className="btn btn-primary">{ submitBtnText }</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                { searchPanel }
            </div>
        )
    }
}