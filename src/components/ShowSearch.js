import React from 'react'

const dontShow = ['players']

const onToggleDropDown = (toggleDropDown) => {
  // do your stuff here
  console.log('toggle dropdown');
  toggleDropDown();
}

const renderSizePerPageDropDown = (props) => {
  return (
    <SizePerPageDropDown
      className='my-size-per-page'
      btnContextual='btn-warning'
      variation='dropup'
      onClick={ () => onToggleDropDown(props.toggleDropDown) }/>
  );
}



const returnTable = ( searchResults, selectRowProp, columType ) => {
    const options = {
        sizePerPageDropDown: renderSizePerPageDropDown
    };

    if (columType === 'player' ) {
        return (
            <BootstrapTable data={searchResults} selectRow={ selectRowProp } striped={true} hover={true}>
                <TableHeaderColumn dataField="id" isKey={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="nickName" dataSort={true}>Nick Name</TableHeaderColumn>
                <TableHeaderColumn dataField="firstName" dataSort={true}>First Name</TableHeaderColumn>
                <TableHeaderColumn dataField="country" dataSort={true}>Country</TableHeaderColumn>
                <TableHeaderColumn dataField="activeTeam" dataSort={true}>Active Team</TableHeaderColumn>
            </BootstrapTable>
        )
    }else if ( columType === 'team' ) {
        return (
            <BootstrapTable data={searchResults} selectRow={ selectRowProp }  striped={true} hover={true}>
                <TableHeaderColumn dataField="id" isKey={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort={true}>Nick Name</TableHeaderColumn>
                <TableHeaderColumn dataField="teamTag" dataSort={true}>Team Tag</TableHeaderColumn>
                <TableHeaderColumn dataField="country" dataSort={true}>Country</TableHeaderColumn>
            </BootstrapTable>
        )
    }else if ( columType === 'tournament' ) {
        return (
            <BootstrapTable data={searchResults} selectRow={ selectRowProp } options={options} pagination striped={true} hover={true}>
                <TableHeaderColumn dataField="id" isKey={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="name" dataSort={true}>Name</TableHeaderColumn>
                <TableHeaderColumn dataField="createdAt" dataSort={true}>Created At</TableHeaderColumn>
                <TableHeaderColumn dataField="startDate" dataSort={true}>Start Date</TableHeaderColumn>
            </BootstrapTable>
        )
    }else if ( columType === 'logs' ) {
        return (
            <BootstrapTable data={searchResults} selectRow={ selectRowProp } options={options} pagination striped={true} hover={true}>
                <TableHeaderColumn dataField="id" width="200" isKey={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="createdAt" width="200" dataSort={true}>Created At</TableHeaderColumn>
                <TableHeaderColumn dataField="type" width="70" dataSort={true}>Type</TableHeaderColumn>
                <TableHeaderColumn dataField="modual" width="90" dataSort={true}>Modual</TableHeaderColumn>
                <TableHeaderColumn dataField="note" dataSort={true}>Note</TableHeaderColumn>
            </BootstrapTable>
        )
    }
}

const ShowSearch = ( searchResults, onClickEvent, selectedId, onClickEditButton, columType, options ) => {
    let editBtnCss = {
        display: 'none'
    }
    if (selectedId !== null)
    {
        editBtnCss = {
            display: 'block'
        }
    }

    const selectRowProp = {
        mode: 'radio',
        onSelect: onClickEvent,
        clickToSelect: true,
        bgColor: '#dbe1ff'
    }
    const editBtnTxt = columType !== 'logs' ? 'Edit' : 'Details'
    return (
        <div className="ShowResults">
            { returnTable( searchResults, selectRowProp, columType, options  ) }
            <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                    <button type="button" onClick={onClickEditButton} style={editBtnCss}  className="btn btn-primary pull-right">{ editBtnTxt }</button>
                </div>
            </div>
        </div>
    )
}

export default ShowSearch