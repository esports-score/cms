import React, { Component } from 'react'
import xhr from 'xhr'
import { Link } from 'react-router-dom'


import AddPlayer from './AddPlayer'
import AddTeam from './AddTeam'
import AddTournament from './AddTournament'
import googleLogin from './googleLogin'

export default class App extends Component {

    constructor( props ) {
        super(props);
        this.state = {
            Autherized: false,
            aptBodyVisible: {
                AddPlayer: false,
                AddTeam: false,
                AddTournament: false,
            }
        };

        this.toggleAddDisplay = this.toggleAddDisplay.bind(this);
    }

    componentWillMount() {
        if (location.hostname === "localhost" ) {
            this.setState({
                Autherized: true
            })
        }else {
            const uri = "http://cms.esports-scores.com/checkSession"
            xhr({
                method: "GET",
                uri: uri,
                headers: {
                    "Accept": "application/json"
                }
            }, function (err, resp, body) {
                if (err)
                {
                    window.location.href = "/login";
                }
                else
                {
                    const Autherized = JSON.parse(resp.body).session
                    this.setState({
                        Autherized: Autherized
                    })
                    if ( !Autherized )
                    {
                        window.location.href = "/login";
                    }
                }
            }.bind(this))
        }
    }

    toggleAddDisplay (Part) {

        const tempVisibility = !this.state.aptBodyVisible[Part],
        aptBodyVisible = this.state.aptBodyVisible;

        if ( Part === 'AddTournament' && tempVisibility )
        {
            aptBodyVisible.AddPlayer = false
            aptBodyVisible.AddTeam = false
        }

        aptBodyVisible[Part] = tempVisibility;

        this.setState({
            aptBodyVisible
        })
    }

    submitForm (query, variables) {
        return new Promise( (resolve, reject) => {
            const body = JSON.stringify({
                  query: query,
                  variables: variables,
                })
            const uri = location.hostname === "cms.esports-scores.com" ? "http://node.esports-scores.com/graphql" : "http://localhost:4000/graphql"
            xhr({
                method: "POST",
                uri: uri,
                body: body,
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                }
            }, function (err, resp, body) {
                err ? reject(err) : resolve(resp)
            })
        })
    }
    searchForm ( query, variables ) {
        return new Promise( (resolve, reject) => {
            const uri = location.hostname === "cms.esports-scores.com" ? "http://node.esports-scores.com/graphql" : "http://localhost:4000/graphql"
            xhr({
                method: "POST",
                uri: uri,
                body: JSON.stringify({
                  query: query,
                  variables: variables,
                }),
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                }
            }, function (err, resp, body) {
                err ? reject(err) : resolve(resp)
            })
        })
    }

    render() {
        if (this.state.Autherized ){
            return(
                <div id="inputSections">

                    <Link to="/logs" style={ { textDecoration: 'none' } }>
                        <div className="panel panel-primary">
                            <div className="panel-heading apt-addheading">
                            <span className="glyphicon glyphicon-plus"></span> Logging</div>
                        </div>
                    </Link>

                    <AddPlayer
                        bodyVisible = { this.state.aptBodyVisible }
                        handleToggle = { this.toggleAddDisplay }
                        submitForm = { this.submitForm }
                        searchForm = { this.searchForm }
                    />

                    <AddTeam
                        bodyVisible = { this.state.aptBodyVisible }
                        handleToggle = { this.toggleAddDisplay }
                        submitForm = { this.submitForm }
                        searchForm = { this.searchForm }
                    />

                    <AddTournament
                        bodyVisible = { this.state.aptBodyVisible }
                        handleToggle = { this.toggleAddDisplay }
                        submitForm = { this.submitForm }
                        searchForm = { this.searchForm }
                    />

                </div>
            )
        }
        else
        {
            return( <div></div>)
        }
    }
}
