import React, { Component } from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import xhr from 'xhr'
import JSONFormatter from 'json-formatter-js'

import { loggingQ } from '../schemas/logging'
import ShowSearch from './ShowSearch'


export default class ShowLogging extends Component
{
    constructor( props )
    {
        super(props);
        this.state = {
            logs : null
        ,   selectedLog : {}
        ,   selectedData : {}
        ,   jsonStyle : {
                background: "#000"
            ,   padding: "10px"
            ,   display: "none"
            }
        ,   jsonHTML: ""
        };

        this.onClickLogRow  = this.onClickLogRow.bind(this)
        this.detailsLogRow  = this.detailsLogRow.bind(this)
    }

    onClickLogRow( nowSelectedData, isSelected, event )
    {
        let {
            selectedLog
        ,   selectedData
        } = this.state

        if ( selectedLog !== selectedData.id || selectedData !== nowSelectedData )
        {
            this.setState({
                selectedLog: selectedData.id,
                selectedData: nowSelectedData
            })
        }
    }

    componentWillMount() {
        this.searchForm( loggingQ )
        .then( (result) =>{
            console.log( 'result >', result )
            const data = JSON.parse(result.body).data.logging
            console.log( 'data >', data )
            this.ShowLogResults( data )
        })
        .catch(error => {
            console.log('onChangeHandeler error >', error)
        })
    }

    ShowLogResults (data)
    {
        if (data.length !== 0 && data[0] !== null )
        {
            this.setState({
                logs: data
            })

        }
        else
        {
            this.setState({
                logs: null
            })
        }
    }

    searchForm ( query, variables ) {
        return new Promise( (resolve, reject) => {
            const uri = location.hostname === "cms.esports-scores.com" ? "http://node.esports-scores.com/graphql" : "http://localhost:4000/graphql"
            xhr({
                method: "POST",
                uri: uri,
                body: JSON.stringify({
                  query: query,
                  variables: variables,
                }),
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                }
            }, function (err, resp, body) {
                err ? reject(err) : resolve(resp)
            })
        })
    }

    detailsLogRow()
    {
        let {
            selectedData
        ,   jsonStyle
        } = this.state

        const myJSON = JSON.parse( selectedData.text )

        const formatter = new JSONFormatter(myJSON, 5, {
            hoverPreviewEnabled: false,
            hoverPreviewArrayCount: 100,
            hoverPreviewFieldCount: 5,
            theme: 'dark',
            animateOpen: true,
            animateClose: true
        });

        const jsonHTML = formatter.render()

        jsonStyle.display = "block"

        this.setState({
            jsonStyle
        ,   jsonHTML
        })

    }

    render() {

        const {
            logs
        ,   selectedLog
        ,   jsonStyle
        ,   jsonHTML
        } = this.state

        let searchLogs = ''

        if (logs !== null )
        {
            searchLogs = ( ShowSearch( logs, this.onClickLogRow, selectedLog, this.detailsLogRow, 'logs' ) )
        }

        return(
            <div className="container">
                <div className="col-md-6 order-md-1 text-center text-md-left pr-md-5">
                    <h1 className="mb-3 bd-text-purple-bright">Logging</h1>
                </div>
                { searchLogs }
                <div dangerouslySetInnerHTML={ { __html:  jsonHTML.outerHTML } } className="col-md-12" id="json" style={ jsonStyle } >

                </div>
            </div>
        )
    }
}