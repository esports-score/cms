import React from 'react'


export const MakeLabelListJSX = ( data, key, onClick, type ) => {
    const style = {
        marginRight: '10px',
        cursor: 'pointer'
    }
    //const labelName = 'Name :'+data.name+'<br>'+
    return (
        <span data-id={key} data-name={type} id={key} onClick={ onClick } style={style} key={`label-list${data.id+data.name}`} className="label label-primary">
            <div data-id={key} data-name={type}>{`Name : ${data.name} `}</div>
            <div data-id={key} data-name={type}>{`Id: ${data.id}`}</div>
        </span>
    )
}