
const InputValidation = (schema, input, hasError, showError) =>
{

    const inputItems = {}

    for (let prop in schema)  {
        if(!schema.hasOwnProperty(prop)) continue;

        const refName = schema[prop].ref,
            tableName = schema[prop].name
        const regexp = typeof schema[prop].regexp === 'object' ? schema[prop].regexp : false

        if ( input[refName].value === '' && schema[prop].required )
        {
            if ( showError )
            {
                schema[prop].error.regexp = false
                schema[prop].error.empty = true
                hasError = true
            }
        }
        else if ( regexp && !regexp.test(input[refName].value) && input[refName].value !== '' )
        {
            if ( showError ){
                schema[prop].error.regexp = true
                schema[prop].error.empty = false
                hasError = true
            }
        }
        else if ( input[refName].value !== '' )
        {
            inputItems[tableName] = input[refName].value
            if ( showError ){
                schema[prop].error.regexp = false
                schema[prop].error.empty = false
            }
        }

    }

    return(
        { inputItems, schema, hasError : hasError }
    )
}

export default InputValidation