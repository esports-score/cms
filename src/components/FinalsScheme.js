import React, { Component } from 'react'
import { ButtonToolbar, SplitButton, MenuItem } from 'react-bootstrap'

export default class FinalsScheme extends Component {

    constructor( props )
    {
        super(props);
        this.state = {
            step: 1,
            finalStage: false,
            errorMsg: "",
            DDfinalStyle: "NONE",
            DDplayoffMatchStyle: "NONE",
            DDfinalMatchStyle: "NONE",
            DDteamsToLB: "NONE",
            stepAnswered: 0,
            forwardBtnStyle : {
                display: "none"
            }
        }

        this.stepsJSX = this.stepsJSX.bind(this)
        this.onClickYNbtn = this.onClickYNbtn.bind(this)
        this.onClickStepBackFwrd = this.onClickStepBackFwrd.bind(this)
        this.selectDDFinalS = this.selectDDFinalS.bind(this)
        this.selectDDteamsLB = this.selectDDteamsLB.bind(this)
    }

    shouldComponentUpdate(nextProps, nextState) {

        if ( !isNaN(nextProps.teamsToFinals) && this.props.teamsToFinals !== nextProps.teamsToFinals)
        {
            return true;
        }

        if (this.props.groupsDone !== nextProps.groupsDone)
        {
            return true;
        }

        if (nextProps.editFinals !== null && this.props.editFinals !== nextProps.editFinals)
        {
            return true;
        }

        if (this.state.step !== nextState.step)
        {
            return true;
        }

        if (this.state.finalStage !== nextState.finalStage)
        {
            return true;
        }

        if (this.state.errorMsg !== nextState.errorMsg)
        {
            return true;
        }

        if (this.state.DDfinalStyle !== nextState.DDfinalStyle)
        {
            return true;
        }

        if (this.state.DDplayoffMatchStyle !== nextState.DDplayoffMatchStyle)
        {
            return true;
        }

        if (this.state.DDfinalMatchStyle !== nextState.DDfinalMatchStyle)
        {
            return true;
        }

        if (this.state.DDteamsToLB !== nextState.DDteamsToLB)
        {
            return true;
        }

        if (this.state.stepAnswered !== nextState.stepAnswered)
        {
            return true;
        }

        if (this.state.forwardBtnStyle !== nextState.forwardBtnStyle)
        {
            return true;
        }

        return false;
    }

    componentDidUpdate(){

        let {
            step,
            finalStage,
            DDfinalStyle,
            DDplayoffMatchStyle,
            DDfinalMatchStyle,
            DDteamsToLB,
            stepAnswered
        } = this.state



        const {
            editFinals
        } = this.props

        if ( step === 2 && !finalStage )
        {
            this.props.returnFinals( { hasFinalStage : finalStage } )
        }
        else if ( step === 4 )
        {

            this.props.returnFinals( {
                hasFinalStage: finalStage,
                finalStageFormat: DDfinalStyle,
                teamsToLowerBracket: DDteamsToLB,
                playoffMatchFormat: DDplayoffMatchStyle,
                finalsMatchFormat: DDfinalMatchStyle
            })
        }
        else if ( stepAnswered === 4 && step < 4 )
        {
            this.props.returnFinals( 'inEdit' )
        }

        if ( editFinals !== null && typeof editFinals.reset === "boolean" && editFinals.reset )
        {
            this.setState({
                step: 1,
                finalStage: false,
                errorMsg: "",
                DDfinalStyle: "NONE",
                DDplayoffMatchStyle: "NONE",
                DDfinalMatchStyle: "NONE",
                DDteamsToLB: "NONE",
                stepAnswered: 0,
                forwardBtnStyle : {
                    display: "none"
                }
            })
            this.props.returnFinals( 'reset' )
        }

        else if ( editFinals !== null && typeof editFinals.error === 'string' )
        {
             this.setState({
                errorMsg: editFinals.error
            })
        }
        else if ( editFinals !== null && typeof editFinals.edit === 'object' )
        {
            finalStage = editFinals.edit.hasFinalStage

            if ( finalStage )
            {
                DDfinalStyle = editFinals.edit.finalStageFormat
                DDteamsToLB = editFinals.edit.teamsToLowerBracket
                DDplayoffMatchStyle = editFinals.edit.playoffMatchFormat
                DDfinalMatchStyle = editFinals.edit.finalsMatchFormat
                stepAnswered = 4
                step = 4
            }
            else
            {
                stepAnswered = 2
                step = 2
            }

            this.setState({
                step,
                finalStage,
                DDfinalStyle,
                DDplayoffMatchStyle,
                DDfinalMatchStyle,
                DDteamsToLB,
                stepAnswered
            })
        }
    }

    posibleGroups(dividend,divisor)
    {
        var result = 0;
        while(dividend >= divisor){
            dividend -= divisor;
            result++;
        }

        return result;
    }

    stepsJSX ()
    {
        const {
            onClickYNbtn,
            selectDDFinalS,
            posibleGroups,
            selectDDteamsLB
        } = this

        const {
            step,
            finalStage,
            DDfinalStyle,
            DDteamsToLB,
            DDplayoffMatchStyle,
            DDfinalMatchStyle
        } = this.state

        const {
            teamsToFinals
        } = this.props

        if ( step === 1 )
        {
            return(
                <div className="col-sm-10">
                    <button className="btn btnYN success" data-val="FinalsYes" onClick={ onClickYNbtn }><span data-val="FinalsYes" className="glyphicon glyphicon-ok success"></span></button>
                    <button className="btn btnYN danger" data-val="FinalsNo" onClick={ onClickYNbtn }><span data-val="FinalsNo" className="glyphicon glyphicon-remove danger"></span></button>
                </div>
            )
        }
        else if ( step === 2 && finalStage )
        {
            return(

                <div className="col-sm-10 form-group">

                    <div className="col-sm-4">
                        <select onChange={ selectDDFinalS  } value={ DDfinalStyle } className="form-control" id="DDfinalStyle">
                            <option defaultValue value="NONE">Final Format</option>
                            <option value="doubleElim">Double Elimination</option>
                            <option value="singleElim">Single Elimination</option>
                        </select>
                    </div>

                    <div className="col-sm-4">
                        <select onChange={ selectDDFinalS  } value={ DDplayoffMatchStyle } className="form-control" id="DDplayoffMatchStyle">
                            <option defaultValue value="NONE">Play Off's Beste Of</option>
                            <option value="BO1">Beste Of One</option>
                            <option value="BO3">Beste Of Three</option>
                            <option value="BO5">Beste Of Five</option>
                        </select>
                    </div>

                    <div className="col-sm-4">
                        <select onChange={ selectDDFinalS  } value={ DDfinalMatchStyle } className="form-control" id="DDfinalMatchStyle">
                            <option defaultValue value="NONE">Finals Beste Of</option>
                            <option value="BO3">Beste Of Three</option>
                            <option value="BO5">Beste Of Five</option>
                            <option value="BO7">Beste Of Seven</option>
                        </select>
                    </div>

                </div>
            )
        }
        else if ( step === 2 && !finalStage  )
        {
            return(
                <div className="col-sm-10">
                    <b>The final is a Separate Event</b>
                </div>
            )
        }
        else if ( step === 3 && DDfinalStyle === 'singleElim' )
        {
            return(
                <div className="col-sm-10">
                    <div className="col-sm-4">Final Bracket: <b>{ DDfinalStyle }</b></div>
                    <div className="col-sm-4">Play off's: <b>{ DDplayoffMatchStyle }</b></div>
                    <div className="col-sm-4">Final: <b>{ DDfinalMatchStyle }</b></div>
                </div>
            )
        }
        else if ( step === 3 && DDfinalStyle === 'doubleElim' )
        {
            const halfTeam = parseInt( teamsToFinals ) / 2
            return(
                <div className="col-sm-10">
                    <div className="form-group">
                        <select onChange={ selectDDteamsLB } value={ DDteamsToLB } className="form-control" id="teamsToLB">
                            <option defaultValue value="NONE">Teams start in lower bracket</option>
                            <option value="0">0</option>
                            <option value={ halfTeam }>{ halfTeam }</option>
                        </select>
                    </div>
                </div>
            )
        }
        else if ( step === 4 )
        {
            return(
                <div className="col-sm-10">
                    <div className="col-sm-4">Final Bracket: <b>{ DDfinalStyle }</b></div>
                    <div className="col-sm-4">Play off's: <b>{ DDplayoffMatchStyle }</b> - Final: <b>{ DDfinalMatchStyle }</b></div>
                    <div className="col-sm-4">Teams to the lowerbracket: <b>{ DDteamsToLB }</b></div>
                </div>
            )
        }
    }

    selectDDFinalS ( event )
    {
        let {
            step,
            stepAnswered,
            DDfinalStyle,
            DDplayoffMatchStyle,
            DDfinalMatchStyle
        } = this.state

        const id = event.target.id

        if ( id === 'DDfinalStyle')
        {
            DDfinalStyle = event.target.value
        }
        else if ( id === 'DDplayoffMatchStyle' )
        {
            DDplayoffMatchStyle = event.target.value
        }
        else if ( id === 'DDfinalMatchStyle' )
        {
            DDfinalMatchStyle = event.target.value
        }

        if ( DDfinalStyle !== "NONE" && DDplayoffMatchStyle !== "NONE" &&  DDfinalMatchStyle !== "NONE" )
        {
            step++
            stepAnswered = stepAnswered < step ? step : stepAnswered
        }

        this.setState({
            step,
            stepAnswered,
            DDfinalStyle,
            DDplayoffMatchStyle,
            DDfinalMatchStyle
        })
    }

    selectDDteamsLB( event )
    {
        let {
            step,
            stepAnswered
        } = this.state

        step++
        stepAnswered = stepAnswered < step ? step : stepAnswered
        this.setState({
            step,
            stepAnswered,
            DDteamsToLB: event.target.value
        })
    }

    onClickYNbtn(event)
    {
        event.preventDefault()
        const idName = event.target.getAttribute('data-val')

        let {
            step,
            finalStage,
            errorMsg,
            stepAnswered
        } = this.state

        const {
            teamsToFinals,
            groupsDone
        } = this.props

        if ( idName === 'FinalsNo' )
        {
            finalStage = false
            errorMsg = ""
            step++
        }
        else if ( teamsToFinals === 0 || isNaN(teamsToFinals) || !groupsDone )
        {
            errorMsg = "Please fill in the amount of teams first and or Group Stage Structure";
        }
        else
        {
            finalStage = true
            errorMsg = ""
            step++
        }

        stepAnswered = stepAnswered < step ? step : stepAnswered

        this.setState({
            step,
            finalStage,
            stepAnswered,
            errorMsg
        })
    }

    onClickStepBackFwrd( event )
    {
        const idName = event.target.getAttribute('data-id')
        event.preventDefault()

        let {
            step,
            forwardBtnStyle,
            stepAnswered
        } = this.state

        if ( idName === 'backBtn' )
        {
            step--
            forwardBtnStyle = {
                display: "inline-block"
            }
        }
        else if ( idName === 'fwrBtn' )
        {
            step++

            if ( step >=  stepAnswered )
            {
                forwardBtnStyle = {
                    display: "none"
                }
            }
        }

        this.setState({
            step,
            forwardBtnStyle
        })
    }

    render() {

        const {
            onClickYNbtn,
            onClickStepBackFwrd,
            stepsJSX
        } = this

        const stepsJSXrtn = stepsJSX()
        const {
            step,
            errorMsg,
            forwardBtnStyle
        } = this.state

        let backBtnStyle = {
            display: "none"
        }

        if ( step > 1 )
        {
            backBtnStyle =  {
                display: "inline-block"
            }
        }

        return (
            <div id="finalStageDiv">
                { stepsJSXrtn }
                <div className="col-sm-2">
                    <button style={ backBtnStyle } data-id="backBtn" className="btn btnYN btn-danger" onClick={ onClickStepBackFwrd }>
                        <span data-id="backBtn" className="glyphicon glyphicon-step-backward"></span>
                    </button>
                    <button style={ forwardBtnStyle } data-id="fwrBtn" className="btn btnYN btn-info" onClick={ onClickStepBackFwrd }>
                        <span data-id="fwrBtn" className="glyphicon glyphicon-step-forward"></span>
                    </button>
                </div>
                <small className="help-block col-sm-12">{ errorMsg }</small>
            </div>
        )
    }
}