export const tournamentsQ = `{
    tournaments{
        id,
        createdAt,
        startDate,
        endDate,
        name,
        description,
        tournament_url,
        leagueid,
        league_tier,
        organizer,
        production,
        format,
        players{
            accountId,
            nickName
        },
        teams{
            steamId,
            name
        },
        matches{
            match_id
        },
        relatedTournaments
        {
            leagueid
        }
    }
}`

export const findTournamentQ = `query( $QueryStr:String! ){
    findTournament(query: $QueryStr ){
        id,
        name,
        createdAt,
        startDate,
        endDate,
        description,
        tournament_url,
        leagueid,
        league_tier,
        organizer,
        production,
        format,
        players{
            accountId,
            nickName
        },
        teams{
            steamId,
            name
        },
        matches{
            match_id
        },
        relatedTournaments
        {
            leagueid
        }
    }
}`

export const searchTournamentQ = `query(
    $Id: ID,
    $name: String,
    $leagueid: String,
){
    searchTournament(input: {
        id:$Id,
        name: $name,
        leagueid: $leagueid,
    }){
        id,
        name,
        createdAt,
        startDate,
        endDate,
        description,
        tournament_url,
        leagueid,
        league_tier,
        organizer,
        production,
        format,
        players{
            accountId,
            nickName
        },
        teams{
            steamId,
            name
        },
        matches{
            match_id
        },
        relatedTournaments
        {
            leagueid
        }
    }
}`


export const addTournamentQ = `mutation(
    $name: String!,
    $description : String!,
    $startDate : String,
    $endDate : String,
    $tournament_url : String!,
    $leagueid : String!,
    $organizer : String,
    $production : String,
    $format: JSON,
    $players: [PlayersId],
    $relatedTournaments : [TournamentsId],
    $teams : [TeamsId],
    $matches : [MatchesId]
){
  addTournament(input: {
    name: $name,
    description : $description,
    startDate : $startDate,
    leagueid : $leagueid,
    endDate : $endDate,
    tournament_url : $tournament_url,
    organizer : $organizer,
    production : $production,
    relatedTournaments : $relatedTournaments,
    players : $players,
    teams : $teams,
    format : $format,
    matches : $matches
  }){
    id,
    name,
    createdAt,
    startDate,
    endDate,
    description,
    tournament_url,
    leagueid,
    league_tier,
    organizer,
    production,
    format,
    players{
        accountId,
        nickName
    },
    teams{
        steamId,
        name
    },
    matches{
        match_id
    },
    relatedTournaments
    {
        leagueid
    }
  }
}`

export const updateTournamentQ = `mutation(
    $Id:ID!,
    $name: String!,
    $leagueid : String!,
    $league_tier : String,
    $startDate: String,
    $endDate: String,
    $description : String!,
    $tournament_url : String!,
    $organizer : String,
    $production : String,
    $format: JSON,
    $relatedTournaments : [TournamentsId],
    $players: [PlayersId],
    $teams : [TeamsId],
    $matches : [MatchesId]
){
  updateTournament( id:$Id, input: {
    name: $name,
    leagueid : $leagueid,
    league_tier : $league_tier,
    description : $description,
    startDate : $startDate,
    endDate : $endDate,
    format: $format,
    tournament_url : $tournament_url,
    organizer : $organizer,
    production : $production,
    relatedTournaments : $relatedTournaments,
    players : $players,
    teams : $teams,
    matches : $matches
  }){
    id,
    name,
    createdAt,
    startDate,
    endDate,
    description,
    tournament_url,
    leagueid,
    league_tier,
    organizer,
    production,
    format,
    players{
        accountId,
        nickName
    },
    teams{
        steamId,
        name
    },
    matches{
        match_id
    },
    relatedTournaments
    {
        leagueid
    }
  }
}`

export const deleteTournament = `mutation( $Id:ID! ){
  deleteTournament( id:$Id )
}`

export const addTournamentSchema = [
{
    name: 'leagueid',
    ref: 'leagueidInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[0-9]+$/,
    placeholder : 'League Id',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'name',
    ref: 'nameInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Tournament Name',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'description',
    ref: 'descriptionInput',
    required : true,
    searchable : false,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Tournament Description',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'tournament_url',
    ref: 'tournament_urlInput',
    required : true,
    searchable : false,
    value: '',
    regexp : /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
    placeholder : 'Tournament URL',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'organizer',
    ref: 'organizerInput',
    required : true,
    searchable : false,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Tournament Organizer',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'production',
    ref: 'productionInput',
    required : true,
    searchable : false,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Tournament Production',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'nrTeams',
    ref: 'nrTeamsInput',
    required : true,
    searchable : false,
    value: '',
    regexp : /^[0-9]+$/,
    placeholder : 'Number of Teams Participating',
    error: {
        empty : false,
        regexp : false
    }
}
]
