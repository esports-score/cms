export const teamQ = `{
    team{
        id,
        name,
        teamTag,
        country,
        steamId,
        url,
        logo,
        players{
            accountId,
            nickName
        }
    }
}`

export const findTeamQ = `query( $QueryStr:String! ){
    findTeam(query: $QueryStr ){
        id,
        name,
        teamTag,
        country,
        steamId,
        url,
        logo,
        players{
            accountId,
            nickName
        },
        matches{
            id
        }
    }
}`

export const searchTeamQ = `query(
    $Id: ID,
    $logo: String,
    $name: String,
    $teamTag: String,
    $steamId: String,
    $url: String
){
    searchTeam(input: {
        id:$Id,
        logo: $logo,
        name: $name,
        teamTag: $teamTag,
        steamId: $steamId,
        url: $url
    }){
        id,
        name,
        teamTag,
        country,
        steamId,
        url,
        logo,
        players{
            accountId,
            nickName
        },
    }
}`

const PlayersId = `input PlayersId{
  accountId: String!
}`

export const addTeamQ = `mutation(
    $logo: String,
    $name: String!,
    $teamTag: String!,
    $country: String!
    $players: [PlayersId],
    $steamId: String!,
    $url: String
){
  addTeam(input: {
    logo: $logo,
    name: $name,
    teamTag: $teamTag,
    country: $country,
    players: $players,
    steamId: $steamId,
    url: $url
  }){
    id,
    name,
    teamTag,
    country,
    steamId,
    url,
    logo,
    players{
        accountId,
        nickName
    },
  }
}`

export const updateTeamQ = `mutation(
    $Id:ID!,
    $logo: String
    $name: String!
    $teamTag: String!
    $players: [PlayersId],
    $steamId: String!
    $url: String
){
  updateTeam( id:$Id, input: {
    logo: $logo,
    name: $name,
    teamTag: $teamTag,
    players: $players,
    steamId: $steamId,
    url: $url
  }){
    id,
    name,
    teamTag,
    country,
    steamId,
    url,
    logo,
    players{
        accountId,
        nickName
    },
  }
}`

export const deleteTeam = `mutation( $Id:ID! ){
  deleteTeam( id:$Id )
}`

export const addTeamSchema = [{
    name: 'name',
    ref: 'nameInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9\ \.]+$/,
    placeholder : 'Team Name',
    error: {
        empty : false,
        regexp : false
    }
},{
    name: 'teamTag',
    ref: 'teamTagInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9_\.]+$/,
    placeholder : 'Team Tag',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'country',
    ref: 'countryInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9\ \.]+$/,
    placeholder : 'Country',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'steamId',
    ref: 'steamIdInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9_\.]+$/,
    placeholder : 'steamId',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'url',
    ref: 'urlInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
    placeholder : 'Team Homepage url',
    error: {
        empty : false,
        regexp : false
    }
},
]
