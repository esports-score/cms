export const playersQ = `{
    players{
        id,
        nickName,
        firstName,
        lastName,
        position,
        country,
        activeTeam,
        steamId,
        accountId,
        twitch,
        active
    }
}`

export const findPlayerQ = `query( $QueryStr:String! ){
    findPlayer(query: $QueryStr ){
        id,
        nickName,
        firstName,
        lastName,
        position,
        country,
        activeTeam,
        steamId,
        accountId,
        twitch,
        active
    }
}`

export const searchPlayerQ = `query(
    $Id: ID,
    $nickName:String,
    $firstName:String,
    $lastName:String,
    $country:String,
    $activeTeam:String,
    $steamId:String,
    $accountId:String,
    $twitch:String
){
    searchPlayer(input: {
        id: $Id,
        nickName: $nickName,
        firstName: $firstName,
        lastName: $lastName,
        country: $country,
        activeTeam: $activeTeam,
        steamId: $steamId,
        accountId: $accountId,
        twitch: $twitch
    }){
        id,
        nickName,
        position,
        firstName,
        lastName,
        country,
        activeTeam,
        steamId,
        accountId,
        twitch,
        active
    }
}`

export const addPlayerQ = `mutation(
    $active:Boolean!,
    $nickName:String!,
    $firstName:String!,
    $lastName:String!,
    $position:String!,
    $country:String!,
    $activeTeam:String!,
    $steamId:String!,
    $accountId:String!,
    $twitch:String
){
  addPlayer(input: {
    active: $active,
    nickName: $nickName,
    firstName: $firstName,
    lastName: $lastName,
    position: $position,
    country: $country
    activeTeam: $activeTeam,
    steamId: $steamId,
    accountId: $accountId,
    twitch: $twitch
  }){
    id,
    active,
    nickName,
    firstName,
    lastName,
    position,
    country,
    activeTeam,
    steamId,
    accountId,
    twitch,
    active
  }
}`

export const updatePlayerQ = `mutation(
    $Id:ID!,
    $active:Boolean!,
    $nickName:String!,
    $firstName:String!,
    $lastName:String!,
    $position:String!,
    $country:String!,
    $activeTeam:String!,
    $steamId:String!,
    $accountId:String!,
    $twitch:String
){
  updatePlayer( id:$Id, input: {
    active: $active,
    nickName: $nickName,
    firstName: $firstName,
    lastName: $lastName,
    position: $position,
    country: $country
    activeTeam: $activeTeam,
    steamId: $steamId,
    accountId: $accountId,
    twitch: $twitch
  }){
    id,
    active,
    nickName,
    firstName,
    lastName,
    position,
    country,
    activeTeam,
    steamId,
    accountId,
    twitch,
    active
  }
}`

export const deletePlayer = `mutation( $Id:ID! ){
  deletePlayer( id:$Id )
}`

export const addPlayerSchema = [{
    name: 'nickName',
    ref: 'nicknameInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Nickname',
    error: {
        empty : false,
        regexp : false
    }
},{
    name: 'firstName',
    ref: 'firstNameInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Firstname',
    error: {
        empty : false,
        regexp : false
    }
},{
    name: 'lastName',
    ref: 'lastNameInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[\s\S]+$/,
    placeholder : 'Lastname',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'position',
    ref: 'positionInput',
    required : true,
    value: '',
    regexp : /^[a-zA-Z0-9\/\.]+$/,
    placeholder : 'Position',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'country',
    ref: 'countryInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9\ \.]+$/,
    placeholder : 'Country',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'activeTeam',
    ref: 'activeTeamInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9\ \.]+$/,
    placeholder : 'Active Team',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'steamId',
    ref: 'steamIdInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[0-9]+$/,
    placeholder : 'Steam Id',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'accountId',
    ref: 'accountIdInput',
    required : true,
    searchable : true,
    value: '',
    regexp : /^[0-9]+$/,
    placeholder : 'Account Id',
    error: {
        empty : false,
        regexp : false
    }
},
{
    name: 'twitch',
    ref: 'twitchInput',
    required : false,
    searchable : true,
    value: '',
    regexp : /^[a-zA-Z0-9_\.]+$/,
    placeholder : 'Twitch',
    error: {
        empty : false,
        regexp : false
    }
}
]
