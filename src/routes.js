import React from 'react';
import { Switch, Route } from 'react-router-dom'

import googleLogin from './components/googleLogin';
import ShowLogging from './components/ShowLogging';
import App from './components/App';
import NotFound from './components/NotFound';

import './assets/stylesheets/react-bootstrap-table-all.min.css'
import './assets/stylesheets/styles.css'

//<Route path='/schedule' component={Schedule}/>

const Routes = (props) => (
    <main>
        <Switch>
            <Route exact path='/' component={App}/>
            <Route exact path='/login' component={googleLogin}/>
            <Route exact path='/logs' component={ShowLogging} />
            <Route path='*' component={NotFound}/>
        </Switch>
    </main>
);

export default Routes;